// tslint:disable:member-ordering
import { convertToParamMap, Data, ParamMap, Params } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 */
export class ActivatedRouteStub {
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  private readonly paramMapSubject = new ReplaySubject<ParamMap>();
  private readonly querySubject = new ReplaySubject<ParamMap>();
  private readonly dataSubject = new Subject<Data>();

  private paramMapValue: ParamMap = convertToParamMap({});
  private queryParamMapValue: ParamMap = convertToParamMap({});
  private dataValue: Data = {};

  /** The mock paramMap observable */
  readonly paramMap = this.paramMapSubject.asObservable();
  readonly queryParamMap = this.querySubject.asObservable();
  readonly data = this.dataSubject.asObservable();

  readonly snapshot = {
    data: this.dataValue,
    paramMap: this.paramMapValue,
    queryParamMap: this.queryParamMapValue
  };

  /** Set the paramMap observables's next value */
  setParamMap(params?: Params): void {
    if (params) {
      this.paramMapValue = convertToParamMap(params);
      this.paramMapSubject.next(this.paramMapValue);
    }
  }
  setQueryParamMap(params?: Params): void {
    if (params) {
      this.queryParamMapValue = convertToParamMap(params);
      this.querySubject.next(this.queryParamMapValue);
    }
  }

  /** Set the paramMap observables's next value */
  setData(data?: Data): void {
    if (data) {
      this.dataValue = data;
      this.dataSubject.next(this.dataValue);
    }
  }
}
