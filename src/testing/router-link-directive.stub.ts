import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[routerLink]'
})
export class RouterLinkDirectiveStub {
  @Input('routerLink') linkParams: string | null = null;
  navigatedTo: string | null = null;

  // tslint:disable-next-line: no-unsafe-any
  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}
