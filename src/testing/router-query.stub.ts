import { Observable, ReplaySubject } from 'rxjs';

export class RouterQueryStub {
  private readonly paramsSubject = new ReplaySubject<string>();

  selectParams(key: string): Observable<string> {
    return this.paramsSubject.asObservable();
  }

  setParams(value: string): void {
    this.paramsSubject.next(value);
  }
}
