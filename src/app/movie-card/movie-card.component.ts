import { Component, Input } from '@angular/core';
import { Movie } from '../state/movie.model';

@Component({
  selector: 'app-movie-card',
  styleUrls: ['./movie-card.component.sass'],
  templateUrl: './movie-card.component.html'
})
export class MovieCardComponent {
  @Input() data: Movie | undefined;
}
