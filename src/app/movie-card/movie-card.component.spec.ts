import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterLinkDirectiveStub } from '../../testing/router-link-directive.stub';
import { movie1 } from '../state/movie.mock';
import { MovieCardComponent } from './movie-card.component';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;
  let linkDebug: DebugElement;
  let routerLink: RouterLinkDirectiveStub;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieCardComponent, RouterLinkDirectiveStub],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    component.data = movie1;

    fixture.detectChanges();

    linkDebug = fixture.debugElement.query(
      By.directive(RouterLinkDirectiveStub)
    );
    routerLink = linkDebug.injector.get(RouterLinkDirectiveStub);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change navigatedTo after click on the link', () => {
    const linkElem: HTMLAnchorElement = linkDebug.nativeElement as HTMLAnchorElement;
    expect(routerLink.navigatedTo).toBeNull();

    linkElem.click();
    expect(routerLink.navigatedTo).toBe('deadpool');
  });
});
