import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MovieCardComponent } from './movie-card.component';

@NgModule({
  declarations: [MovieCardComponent],
  exports: [MovieCardComponent],
  imports: [CommonModule, MatCardModule, MatIconModule, RouterModule]
})
export class MovieCardModule {}
