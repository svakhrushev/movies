import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, throwError } from 'rxjs';
import { catchError, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { DialogService } from '../dialogs/dialog.service';
import { GenreType } from '../genres/genres.model';
import { GenresService } from '../genres/genres.service';
import { MovieQuery } from '../state/movie.query';
import { MovieService } from '../state/movie.service';

interface LocationState {
  navigationId?: number;
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-movie',
  styleUrls: ['./movie.component.sass'],
  templateUrl: './movie.component.html'
})
export class MovieComponent implements OnInit, OnDestroy {
  data$ = this.movieQuery.selectedMovie$;
  private readonly destroy$ = new Subject();

  constructor(
    private readonly genresService: GenresService,
    private readonly movieQuery: MovieQuery,
    private readonly moviesService: MovieService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly location: Location,
    private readonly dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap
      .pipe(
        takeUntil(this.destroy$),
        map(params => params.get('key')),
        switchMap(key =>
          key === null
            ? throwError(new Error('Something goes wrong'))
            : this.moviesService.getByKey(key)
        ),
        catchError((error: Error) =>
          this.dialogService.showError(error.message).pipe(
            tap(() => {
              this.goBack();
            })
          )
        )
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getGenre(genre: GenreType): string {
    return this.genresService.getGenre(genre);
  }

  goBack(): void {
    this.router.navigate(['/']);
  }

  /* goBack(): void {
    const locationState = this.location.getState() as LocationState | undefined;
    const canBack =
      locationState !== undefined &&
      locationState.navigationId !== undefined &&
      locationState.navigationId > 1;

    if (canBack) {
      this.location.back();
    } else {
      this.router.navigate(['/']);
    }
  } */
}
