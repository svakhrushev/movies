import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DialogsModule } from '../dialogs/dialogs.module';
import { RateModule } from '../rate/rate.module';
import { MovieRoutingModule } from './movie-routing.module';
import { MovieComponent } from './movie.component';

@NgModule({
  declarations: [MovieComponent],
  imports: [
    CommonModule,
    MovieRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    RateModule,
    DialogsModule
  ]
})
export class MovieModule {}
