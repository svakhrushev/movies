import { Location } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { ActivatedRouteStub } from '../../testing/activated-route.stub';
import { DialogService } from '../dialogs/dialog.service';
import { GenreType } from '../genres/genres.model';
import { GenresService } from '../genres/genres.service';
import { movie1 } from '../state/movie.mock';
import { MovieQuery } from '../state/movie.query';
import { MovieService } from '../state/movie.service';
import { MovieComponent } from './movie.component';

const GenresServiceStub: Partial<GenresService> = {
  getGenre: (genre: GenreType) => 'Action'
};
const MovieQueryStub: Partial<MovieQuery> = {
  selectedMovie$: of(movie1)
};

describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;
  let activatedRoute: ActivatedRouteStub;
  let movieService: jasmine.SpyObj<MovieService>;
  let location: jasmine.SpyObj<Location>;
  let router: jasmine.SpyObj<Router>;
  let dialogService: jasmine.SpyObj<DialogService>;

  beforeEach(async(() => {
    const MovieServiceSpy = jasmine.createSpyObj('MovieService', ['getByKey']);
    const RouterSpy = jasmine.createSpyObj('Router', ['navigate']);
    const LocationSpy = jasmine.createSpyObj('Location', ['getState', 'back']);
    const DialogServiceSpy = jasmine.createSpyObj('DialogService', [
      'showError'
    ]);

    TestBed.configureTestingModule({
      declarations: [MovieComponent],
      providers: [
        { provide: GenresService, useValue: GenresServiceStub },
        { provide: MovieQuery, useValue: MovieQueryStub },
        { provide: MovieService, useValue: MovieServiceSpy },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: Router, useValue: RouterSpy },
        { provide: Location, useValue: LocationSpy },
        { provide: DialogService, useValue: DialogServiceSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    activatedRoute = fixture.debugElement.injector.get<unknown>(
      ActivatedRoute
    ) as ActivatedRouteStub;
    movieService = TestBed.get(MovieService) as jasmine.SpyObj<MovieService>;
    location = TestBed.get(Location) as jasmine.SpyObj<Location>;
    router = TestBed.get(Router) as jasmine.SpyObj<Router>;
    dialogService = TestBed.get(DialogService) as jasmine.SpyObj<DialogService>;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call moviesService.getByKey', () => {
    movieService.getByKey.and.returnValue(of(movie1));
    activatedRoute.setParamMap({ key: 'deadpool' });
    fixture.detectChanges();

    expect(movieService.getByKey).toHaveBeenCalledWith('deadpool');
  });

  it('should go to the previous page', () => {
    location.getState.and.returnValue({ navigationId: 2 });
    const backBtn: HTMLButtonElement = fixture.debugElement.query(
      By.css('.back-btn')
    ).nativeElement as HTMLButtonElement;

    backBtn.click();

    expect(location.back).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('should go to the movies list page if here is no history', () => {
    const backBtn: HTMLButtonElement = fixture.debugElement.query(
      By.css('.back-btn')
    ).nativeElement as HTMLButtonElement;

    backBtn.click();

    expect(location.back).not.toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('should open error dialog if there is a error when getting the data', () => {
    movieService.getByKey.and.returnValue(
      throwError(new Error('Movie with key "fake" was not found'))
    );
    dialogService.showError.and.returnValue(of(true));
    activatedRoute.setParamMap({ key: 'fake' });
    fixture.detectChanges();

    expect(movieService.getByKey).toHaveBeenCalledWith('fake');
    expect(dialogService.showError).toHaveBeenCalledWith(
      'Movie with key "fake" was not found'
    );
  });

  it('should open error dialog if there is not the key', () => {
    dialogService.showError.and.returnValue(of(true));
    activatedRoute.setParamMap({ key: null });
    fixture.detectChanges();

    expect(movieService.getByKey).not.toHaveBeenCalled();
    expect(dialogService.showError).toHaveBeenCalledWith(
      'Something goes wrong'
    );
  });
});
