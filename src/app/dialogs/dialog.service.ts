import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ErrorDialog } from './error/error-dialog.component';

@Injectable()
export class DialogService {
  constructor(private readonly dialog: MatDialog) {}

  showError(message: string): Observable<unknown> {
    const dialogRef = this.dialog.open(ErrorDialog, {
      data: message
    });

    return dialogRef.afterClosed();
  }
}
