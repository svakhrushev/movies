import { TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { of } from 'rxjs';
import { DialogService } from './dialog.service';

const dialogStub: Partial<MatDialogRef<unknown>> = {
  afterClosed: () => of(true)
};

describe('DialogService', () => {
  let service: DialogService;
  let matDialogSpy: jasmine.SpyObj<MatDialog>;

  beforeEach(() => {
    const MatDialogSpy = jasmine.createSpyObj('MatDialog', ['open']);

    TestBed.configureTestingModule({
      providers: [DialogService, { provide: MatDialog, useValue: MatDialogSpy }]
    });

    service = TestBed.get(DialogService) as DialogService;
    matDialogSpy = TestBed.get(MatDialog) as jasmine.SpyObj<MatDialog>;

    matDialogSpy.open.and.returnValues(
      dialogStub as MatDialogRef<unknown, unknown>
    );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call "dialog.open" with correct arguments when a error dialog is opened', () => {
    service.showError('message');

    expect(matDialogSpy.open).toHaveBeenCalledWith(jasmine.any(Function), {
      data: 'message'
    });
  });
});
