import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogService } from './dialog.service';
import { ErrorDialogModule } from './error/error-dialog.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ErrorDialogModule, MatDialogModule],
  providers: [DialogService]
})
export class DialogsModule {}
