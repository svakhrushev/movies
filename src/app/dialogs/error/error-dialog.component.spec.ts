import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef
} from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { ErrorDialog } from './error-dialog.component';

describe('ErrorDialog', () => {
  let component: ErrorDialog;
  let fixture: ComponentFixture<ErrorDialog>;
  let matDialogRef: jasmine.SpyObj<MatDialogRef<ErrorDialog>>;

  beforeEach(async(() => {
    const MatDialogRefSpy = jasmine.createSpyObj('MatDialogRef', ['close']);

    TestBed.configureTestingModule({
      declarations: [ErrorDialog],
      imports: [MatDialogModule],
      providers: [
        { provide: MatDialogRef, useValue: MatDialogRefSpy },
        { provide: MAT_DIALOG_DATA, useValue: 'error message' }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    matDialogRef = TestBed.get(MatDialogRef) as jasmine.SpyObj<
      MatDialogRef<ErrorDialog>
    >;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct message', () => {
    const header: HTMLHeadElement = fixture.debugElement.query(By.css('h1'))
      .nativeElement as HTMLHeadElement;
    const message: HTMLParagraphElement = fixture.debugElement.query(
      By.css('p')
    ).nativeElement as HTMLParagraphElement;

    expect(header.textContent).toBe('Error');
    expect(message.textContent).toBe('error message');
  });

  it('should call dialogRef.close when dialog is closed', () => {
    const cancelBtn: HTMLButtonElement = fixture.debugElement.queryAll(
      By.css('button')
    )[0].nativeElement as HTMLButtonElement;
    cancelBtn.click();

    expect(matDialogRef.close).toHaveBeenCalledWith();
  });
});
