import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { ErrorDialog } from './error-dialog.component';

@NgModule({
  declarations: [ErrorDialog],
  entryComponents: [ErrorDialog],
  exports: [ErrorDialog],
  imports: [CommonModule, MatButtonModule]
})
export class ErrorDialogModule {}
