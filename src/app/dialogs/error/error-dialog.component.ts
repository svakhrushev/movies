import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-error',
  styleUrls: ['./error-dialog.component.sass'],
  templateUrl: './error-dialog.component.html'
})
export class ErrorDialog {
  message = this.data;

  constructor(
    private readonly dialogRef: MatDialogRef<ErrorDialog>,
    @Inject(MAT_DIALOG_DATA) private readonly data: string
  ) {}

  close(): void {
    this.dialogRef.close();
  }
}
