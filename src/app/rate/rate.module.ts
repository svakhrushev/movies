import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RateComponent } from './rate.component';
import { RateService } from './rate.service';

@NgModule({
  declarations: [RateComponent],
  exports: [RateComponent],
  imports: [CommonModule, MatIconModule],
  providers: [RateService]
})
export class RateModule {}
