import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RateComponent } from './rate.component';
import { RateService } from './rate.service';

@Component({
  template: `
    <app-rate [value]="rateValue"></app-rate>
  `
})
class WrapperComponent {
  rateValue = 0;
}

describe('RateComponent', () => {
  let fixture: ComponentFixture<WrapperComponent>;
  let wrapperComponentDE: DebugElement;
  let wrapperComponent: WrapperComponent;
  let componentDE: DebugElement;
  let component: RateComponent;
  let rateService: jasmine.SpyObj<RateService>;

  beforeEach(async(() => {
    const RateServiceSpy = jasmine.createSpyObj('RateService', ['getRate']);

    TestBed.configureTestingModule({
      declarations: [RateComponent, WrapperComponent],
      providers: [{ provide: RateService, useValue: RateServiceSpy }],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    wrapperComponentDE = fixture.debugElement;
    wrapperComponent = fixture.componentInstance;
    componentDE = wrapperComponentDE.query(By.css('app-rate'));
    component = componentDE.componentInstance as RateComponent;
    rateService = TestBed.get(RateService) as jasmine.SpyObj<RateService>;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
