import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Rate, RateService } from './rate.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-rate',
  styleUrls: ['./rate.component.sass'],
  templateUrl: './rate.component.html'
})
export class RateComponent implements OnChanges {
  @Input() value = 0;
  private readonly DEF_RATE: Rate = this.rateService.getRate(0);
  private readonly rateSubject = new BehaviorSubject<Rate>(this.DEF_RATE);

  constructor(private readonly rateService: RateService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.setRate(changes.value.currentValue as number);
  }

  get rate$(): Observable<Rate> {
    return this.rateSubject.asObservable();
  }

  private setRate(value: number): void {
    const rate = this.rateService.getRate(value);
    this.rateSubject.next(rate);
  }
}
