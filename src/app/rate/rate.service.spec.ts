// tslint:disable:no-magic-numbers
import { TestBed } from '@angular/core/testing';
import { RateIcon, RateService } from './rate.service';

describe('RateService', () => {
  let service: RateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RateService]
    });
    service = TestBed.get(RateService) as RateService;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return 5-0-5 for rate 5.1', () => {
    expect(service.getRate(5.1)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });

  it('should return 4-0-6 for rate 3.8', () => {
    expect(service.getRate(3.8)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });

  it('should return 0-0-10 for rate -1', () => {
    expect(service.getRate(-1)).toEqual([
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });

  it('should return 10-0-0 for rate 11', () => {
    expect(service.getRate(11)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR
    ]);
  });

  it('should return 7-0-3 for rate 6.75', () => {
    expect(service.getRate(6.75)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });

  it('should return 2-0-8 for rate 2.25', () => {
    expect(service.getRate(2.25)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });

  it('should return 3-1-6 for rate 3.26', () => {
    expect(service.getRate(3.26)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_HALF,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });

  it('should return 9-1-0 for rate 9.7', () => {
    expect(service.getRate(9.7)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_HALF
    ]);
  });

  it('should return 5-1-4 for rate 5.5', () => {
    expect(service.getRate(5.5)).toEqual([
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR,
      RateIcon.STAR_HALF,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER,
      RateIcon.STAR_BORDER
    ]);
  });
});
