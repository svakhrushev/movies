import { Injectable } from '@angular/core';

export enum RateIcon {
  STAR = 'star',
  STAR_HALF = 'star_half',
  STAR_BORDER = 'star_border'
}

export type Rate = RateIcon[];

const MAX_RATE = 10;
const DEVIATION = 0.25;

@Injectable()
export class RateService {

  getRate(value: number): Rate {
    const newValue = Math.max(0, Math.min(MAX_RATE, value));
    let full = Math.floor(newValue + DEVIATION);
    let empty = MAX_RATE - Math.ceil(newValue - DEVIATION);
    let half = MAX_RATE - full - empty;

    const rate: Rate = [];
    while (full-- > 0) {
      rate.push(RateIcon.STAR);
    }
    while (half-- > 0) {
      rate.push(RateIcon.STAR_HALF);
    }
    while (empty-- > 0) {
      rate.push(RateIcon.STAR_BORDER);
    }

    return rate;
  }
}
