import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';

const routes: Routes = [
  {
    component: MoviesComponent,
    data: { animation: 'MoviesPage' },
    path: '',
    pathMatch: 'full'
  },
  {
    loadChildren: () => import('./movie/movie.module').then(m => m.MovieModule),
    path: ':key'
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ]
})
export class AppRoutingModule {}
