import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatMenu } from '@angular/material/menu';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-filter',
  styleUrls: ['./filter.component.sass'],
  templateUrl: './filter.component.html'
})
export class FilterComponent implements OnChanges {
  @ViewChild('filterMenu', { static: true }) filterMenu: MatMenu | undefined;

  @Input() labelKey: string = '';
  @Input() list: Array<Record<string, string>> = [];
  @Input() valueKey: string = '';
  @Input() values: string[] = [];
  @Output() changeSelection = new EventEmitter<string[]>();

  valuesMap: Record<string, boolean> = {};

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('values')) {
      this.valuesMap = {};
      const values = changes.values.currentValue as string[];

      values.forEach(value => {
        this.valuesMap[value] = true;
      });
    }
  }

  changeModel(event: MatCheckboxChange, value: string): void {
    this.valuesMap[value] = event.checked;
    const values = Object.entries(this.valuesMap)
      .filter(valueMap => valueMap[1])
      .map(valueMap => valueMap[0])
      .sort();
    this.changeSelection.emit(values);
  }

  clearFilters(): void {
    this.changeSelection.emit([]);
  }
}
