import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCheckbox, MatCheckboxModule } from '@angular/material/checkbox';
import { By } from '@angular/platform-browser';
import { GenreType } from '../genres/genres.model';
import { FilterComponent } from './filter.component';

@Component({
  template: `
    <app-filter
      [list]="genres"
      labelKey="key"
      valueKey="value"
      [values]="values"
      (changeSelection)="filterChanged($event)"
    ></app-filter>
  `
})
class WrapperComponent {
  genres = [
    { key: 'Action', value: GenreType.Action },
    { key: 'Adventure', value: GenreType.Adventure },
    { key: 'Biography', value: GenreType.Biography }
  ];
  values = ['action'];

  // tslint:disable-next-line:no-empty
  filterChanged(value: GenreType[]): void {}
}

describe('FilterComponent', () => {
  let fixture: ComponentFixture<WrapperComponent>;
  let wrapperComponentDE: DebugElement;
  let wrapperComponent: WrapperComponent;
  let componentDE: DebugElement;
  let component: FilterComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilterComponent, WrapperComponent],
      imports: [MatCheckboxModule],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    wrapperComponentDE = fixture.debugElement;
    wrapperComponent = fixture.componentInstance;
    componentDE = wrapperComponentDE.query(By.css('app-filter'));
    component = componentDE.componentInstance as FilterComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return new list of value when select an item', () => {
    const filterChangedSpy = spyOn(wrapperComponent, 'filterChanged');
    const checkbox = componentDE.queryAll(By.css('mat-checkbox'))[1]
      .componentInstance as MatCheckbox;

    checkbox.change.emit({ checked: true, source: checkbox });

    expect(filterChangedSpy).toHaveBeenCalledWith(['action', 'adventure']);
  });

  it('should clear filters wnek click on "Clear filters" button', () => {
    const filterChangedSpy = spyOn(wrapperComponent, 'filterChanged');
    const clearBtn = componentDE.query(By.css('button'))
      .nativeElement as HTMLButtonElement;

    clearBtn.click();

    expect(filterChangedSpy).toHaveBeenCalledWith([]);
  });
});
