import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FilterModule } from '../filter/filter.module';
import { MovieCardModule } from '../movie-card/movie-card.module';
import { SearchFieldModule } from '../search-field/search-field.module';
import { MoviesComponent } from './movies.component';

@NgModule({
  declarations: [MoviesComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    SearchFieldModule,
    FilterModule,
    MovieCardModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ]
})
export class MoviesModule {}
