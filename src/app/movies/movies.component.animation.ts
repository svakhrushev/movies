import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

export const openCloseFilter = trigger('openCloseFilter', [
  state(
    'open',
    style({
      transform: 'translateY(0%)'
    })
  ),
  state(
    'closed',
    style({
      transform: 'translateY(-100%)'
    })
  ),
  transition('open => closed', [animate('0.3s ease-out')]),
  transition('closed => open', [animate('0.3s ease-in')])
]);
