import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GenreType } from '../genres/genres.model';
import { GenresService } from '../genres/genres.service';
import { MovieQuery } from '../state/movie.query';
import { MovieService } from '../state/movie.service';
import { MovieUIService } from '../state/movie.ui.service';
import { openCloseFilter } from './movies.component.animation';

const duration = 200;
@Component({
  animations: [
    openCloseFilter,
    trigger('flyInOut', [
      transition('void => *', [
        style({ transform: 'scale(0)', opacity: 0 }),
        animate(duration, style({ transform: 'scale(1)', opacity: 1 }))
      ]),
      transition('* => void', [
        style({ transform: 'scale(1)', opacity: 1 }),
        animate(duration, style({ transform: 'scale(0)', opacity: 0 }))
      ])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-movies',
  styleUrls: ['./movies.component.sass'],
  templateUrl: './movies.component.html'
})
export class MoviesComponent implements OnInit, OnDestroy {
  movies$ = this.moviesQuery.selectFilteredMovie();
  count$ = this.moviesQuery.selectFilteredMovieCount();
  filter$ = this.moviesQuery.selectFilter();
  selectedGenres$ = this.moviesQuery.selectGenres();
  loading$ = this.moviesQuery.selectLoading();
  genres = this.genresService.genresEntries;
  isFilterOpen = false;

  private readonly destroy$ = new Subject();

  constructor(
    private readonly moviesService: MovieService,
    private readonly moviesUIService: MovieUIService,
    private readonly moviesQuery: MovieQuery,
    private readonly genresService: GenresService
  ) {}

  ngOnInit(): void {
    this.moviesService
      .getAll()
      .pipe(takeUntil(this.destroy$))
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterChanged(value: GenreType[]): void {
    this.moviesUIService.setSelectedGenres(value);
  }

  searchChanged(value: string): void {
    this.moviesUIService.setFilter(value);
  }

  toggleFilters(): void {
    this.isFilterOpen = !this.isFilterOpen;
  }
}
