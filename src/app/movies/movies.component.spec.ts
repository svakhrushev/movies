import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { GenreType } from '../genres/genres.model';
import { GenresService } from '../genres/genres.service';
import { movies } from '../state/movie.mock';
import { MovieQuery } from '../state/movie.query';
import { MovieService } from '../state/movie.service';
import { MovieUIService } from '../state/movie.ui.service';
import { MoviesComponent } from './movies.component';

const GenresServiceStub: Partial<GenresService> = {
  genresEntries: [
    { key: 'Action', value: GenreType.Action },
    { key: 'Adventure', value: GenreType.Adventure }
  ]
};
const MovieServiceStub: Partial<MovieService> = {
  getAll: () => of(movies)
};

describe('MoviesComponent', () => {
  let component: MoviesComponent;
  let fixture: ComponentFixture<MoviesComponent>;
  let movieUIService: jasmine.SpyObj<MovieUIService>;

  beforeEach(async(() => {
    const MovieUIServiceSpy = jasmine.createSpyObj('MovieUIService', [
      'setSelectedGenres',
      'setFilter'
    ]);
    const MovieQuerySpy = jasmine.createSpyObj('MovieQuery', [
      'selectFilteredMovie',
      'selectFilter',
      'selectGenres',
      'selectFilteredMovieCount',
      'selectLoading'
    ]);

    TestBed.configureTestingModule({
      declarations: [MoviesComponent],
      imports: [NoopAnimationsModule],
      providers: [
        { provide: MovieService, useValue: MovieServiceStub },
        { provide: MovieUIService, useValue: MovieUIServiceSpy },
        { provide: MovieQuery, useValue: MovieQuerySpy },
        { provide: GenresService, useValue: GenresServiceStub }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesComponent);
    component = fixture.componentInstance;
    movieUIService = TestBed.get(MovieUIService) as jasmine.SpyObj<
      MovieUIService
    >;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call moviesUIService.setSelectedGenres when the filter is changed', () => {
    component.filterChanged([GenreType.Action]);
    component.filterChanged([GenreType.Action, GenreType.Comedy]);
    component.filterChanged([]);

    expect(movieUIService.setSelectedGenres.calls.allArgs()).toEqual([
      [[GenreType.Action]],
      [[GenreType.Action, GenreType.Comedy]],
      [[]]
    ]);
  });

  it('should call moviesUIService.setFilter when the search value is changed', () => {
    component.searchChanged('filter');
    component.searchChanged('new value');
    component.searchChanged('');

    expect(movieUIService.setFilter.calls.allArgs()).toEqual([
      ['filter'],
      ['new value'],
      ['']
    ]);
  });
});
