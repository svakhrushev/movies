import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-search-field',
  styleUrls: ['./search-field.component.sass'],
  templateUrl: './search-field.component.html'
})
export class SearchFieldComponent {
  @Input() value = '';
  @Output() changeSearch = new EventEmitter<string>();

  changeModel(value: string = ''): void {
    this.changeSearch.emit(value);
  }
}
