import { Component, DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { SearchFieldComponent } from './search-field.component';

@Component({
  template: `
    <app-search-field
      [value]="filter"
      (changeSearch)="searchChanged($event)"
    ></app-search-field>
  `
})
class WrapperComponent {
  filter = '';
  // tslint:disable-next-line: no-empty
  searchChanged(value: string): void {}
}

describe('SearchFieldComponent', () => {
  let fixture: ComponentFixture<WrapperComponent>;
  let wrapperComponentDE: DebugElement;
  let wrapperComponent: WrapperComponent;
  let componentDE: DebugElement;
  let component: SearchFieldComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFieldComponent, WrapperComponent],
      imports: [FormsModule],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    wrapperComponentDE = fixture.debugElement;
    wrapperComponent = fixture.componentInstance;
    componentDE = wrapperComponentDE.query(By.css('app-search-field'));
    component = componentDE.componentInstance as SearchFieldComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit new value of search', fakeAsync(() => {
    // const searchChangedSpy = spyOn(component.changeSearch, 'emit');
    const searchChangedSpy = spyOn(wrapperComponent, 'searchChanged');
    const input = getInput();

    wrapperComponent.filter = 'filter';
    fixture.detectChanges();
    tick();
    input.dispatchEvent(new Event('input'));

    wrapperComponent.filter = 'test';
    fixture.detectChanges();
    tick();
    input.dispatchEvent(new Event('input'));

    wrapperComponent.filter = '';
    fixture.detectChanges();
    tick();
    input.dispatchEvent(new Event('input'));

    wrapperComponent.filter = 'new value';
    fixture.detectChanges();
    tick();
    input.dispatchEvent(new Event('input'));

    expect(searchChangedSpy.calls.allArgs()).toEqual([
      ['filter'],
      ['test'],
      [''],
      ['new value']
    ]);
  }));

  it('should display Clear button if the input is not empty', () => {
    wrapperComponent.filter = 'filter';
    fixture.detectChanges();

    const clearBtn = getClearButton();
    expect(clearBtn).toBeTruthy();
  });

  it('should clear input when click on Clear button', () => {
    const changeModelSpy = spyOn(component.changeSearch, 'emit');
    wrapperComponent.filter = 'filter';
    fixture.detectChanges();

    const clearBtn = getClearButton();
    clearBtn.click();
    fixture.detectChanges();

    const input = getInput();
    expect(input.value).toBe('');
    expect(changeModelSpy).toHaveBeenCalledWith('');
  });

  function getInput(): HTMLInputElement {
    return componentDE.query(By.css('input')).nativeElement as HTMLInputElement;
  }

  function getClearButton(): HTMLButtonElement {
    return componentDE.query(By.css('button'))
      .nativeElement as HTMLButtonElement;
  }
});
