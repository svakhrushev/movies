import {
  animate,
  animateChild,
  group,
  query,
  style,
  transition,
  trigger
} from '@angular/animations';

export const slideInAnimation = trigger('routeAnimations', [
  transition('MoviesPage => MovieDetailPage', [
    style({ position: 'relative' }),
    query(':enter, :leave', [
      style({
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
        'z-index': 1
      })
    ]),
    query(':enter', [style({ left: '100%', 'z-index': 2 })]),
    query(':leave', [style({ 'will-change': 'left' }), animateChild()]),
    group([
      query(':enter', [animate('300ms ease-out', style({ left: '0%' }))])
    ]),
    query(':enter', animateChild())
  ]),
  transition('MovieDetailPage => MoviesPage', [
    style({ position: 'relative' }),
    query(':enter, :leave', [
      style({
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
        'z-index': 1
      })
    ]),
    query(':leave', [style({ 'will-change': 'left' }), animateChild()]),
    group([
      query(':leave', [animate('300ms ease-out', style({ left: '100%' }))])
    ]),
    query(':enter', animateChild())
  ])
]);
