import { Inject, Injectable } from '@angular/core';
import { EMPTY, Observable, of, throwError } from 'rxjs';
import { startWith, switchMapTo, tap } from 'rxjs/operators';
import { Movie } from './movie.model';
import { MovieQuery } from './movie.query';
import { MovieStore } from './movie.store';
import { MOVIES } from './movie.token.js';

@Injectable()
export class MovieService {
  constructor(
    private readonly moviesStore: MovieStore,
    private readonly moviesQuery: MovieQuery,
    @Inject(MOVIES) private readonly movies: Movie[]
  ) {}

  getAll(): Observable<Movie[]> {
    const request$ = EMPTY.pipe(
      startWith(true),
      tap(() => {
        this.moviesStore.setLoading(true);
      }),
      switchMapTo(of(this.movies)),
      tap(entities => {
        this.moviesStore.set(entities);
      })
    );
    return this.moviesQuery.getHasCache()
      ? of(this.moviesQuery.getAll())
      : request$;
  }

  getByKey(key: string): Observable<Movie> {
    const movie: Movie | undefined = this.movies.find(item => item.key === key);
    if (!movie) {
      return throwError(new Error(`Movie with key "${key}" was not found`));
    }

    const request$ = of(movie).pipe(
      tap(entity => {
        this.moviesStore.upsert(key, entity);
      })
    );
    return this.moviesQuery.getHasCache()
      ? of(this.moviesQuery.getEntity(key))
      : request$;
  }
}
