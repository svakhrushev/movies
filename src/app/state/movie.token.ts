import { InjectionToken } from '@angular/core';
import { default as moviesList } from '../../mocks/movie.mock-data.json';
import { Movie } from './movie.model';

export const movies = moviesList as Movie[];
export const MOVIES = new InjectionToken<Movie[]>('MOVIES');
