import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { GenreType } from '../genres/genres.model';
import { Movie } from './movie.model';

export interface MovieUI {
  filter: string;
  selectedGenres: GenreType[];
}

export interface MovieState extends EntityState<Movie> {}
export interface MovieUIState extends EntityState<MovieUI> {}

const initUIStore: MovieUI = {
  filter: '',
  selectedGenres: []
};

const initState: MovieState = {};

@Injectable()
@StoreConfig({
  cache: {
    ttl: 3600000
  },
  idKey: 'key',
  name: 'movies'
})
export class MovieStore extends EntityStore<MovieState> {
  constructor() {
    super(initState);
    this.createUIStore(initUIStore);
  }

  setFilter(filter: string): void {
    this.ui.update({ filter });
  }

  setGenres(value: GenreType[]): void {
    this.ui.update({ selectedGenres: value });
  }
}
