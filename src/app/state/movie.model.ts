import { GenreType } from '../genres/genres.model';

export interface Movie {
  id: number;
  key: string;
  name: string;
  description: string;
  genres: GenreType[];
  img: string;
  length: string;
  rate: string;
}