import { TestBed } from '@angular/core/testing';
import { GenreType } from '../genres/genres.model';
import { MovieStore } from './movie.store';
import { MovieUIService } from './movie.ui.service';

const MovieStoreSpy = jasmine.createSpyObj('MovieStore', [
  'setFilter',
  'setGenres'
]);

describe('MovieUIService', () => {
  let service: MovieUIService;
  let movieStore: jasmine.SpyObj<MovieStore>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MovieUIService,
        { provide: MovieStore, useValue: MovieStoreSpy }
      ]
    });

    service = TestBed.get(MovieUIService) as MovieUIService;
    movieStore = TestBed.get(MovieStore) as jasmine.SpyObj<MovieStore>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call moviesStore.setFilter when set filter', () => {
    service.setFilter('filter');
    service.setFilter();
    service.setFilter('new value');
    service.setFilter('');

    expect(movieStore.setFilter.calls.allArgs()).toEqual([
      ['filter'],
      [''],
      ['new value'],
      ['']
    ]);
  });

  it('should call moviesStore.setGenres when select genre', () => {
    service.setSelectedGenres([GenreType.Action]);
    service.setSelectedGenres([GenreType.Action, GenreType.Biography]);
    service.setSelectedGenres([]);

    expect(movieStore.setGenres.calls.allArgs()).toEqual([
      [[GenreType.Action]],
      [[GenreType.Action, GenreType.Biography]],
      [[]]
    ]);
  });
});
