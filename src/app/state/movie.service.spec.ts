// tslint:disable: no-unsafe-any
import { TestBed } from '@angular/core/testing';
import { cold } from 'jasmine-marbles';
import { movie4, movies } from './movie.mock';
import { MovieQuery } from './movie.query';
import { MovieService } from './movie.service';
import { MovieStore } from './movie.store';
import { MOVIES } from './movie.token';

describe('MovieService', () => {
  let service: MovieService;
  let moviesStore: jasmine.SpyObj<MovieStore>;
  let moviesQuery: jasmine.SpyObj<MovieQuery>;

  beforeEach(() => {
    const MovieStoreSpy = jasmine.createSpyObj('MovieStore', [
      'set',
      'upsert',
      'setLoading'
    ]);
    const MovieQuerySpy = jasmine.createSpyObj('MovieQuery', [
      'getHasCache',
      'getAll',
      'getEntity'
    ]);

    TestBed.configureTestingModule({
      providers: [
        { provide: MovieStore, useValue: MovieStoreSpy },
        { provide: MovieQuery, useValue: MovieQuerySpy },
        { provide: MOVIES, useValue: movies },
        {
          deps: [MovieStore, MovieQuery, MOVIES],
          provide: MovieService,
          useClass: MovieService
        }
      ]
    });

    service = TestBed.get(MovieService) as MovieService;
    moviesStore = TestBed.get(MovieStore) as jasmine.SpyObj<MovieStore>;
    moviesQuery = TestBed.get(MovieQuery) as jasmine.SpyObj<MovieQuery>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set store and return all movies when call getAll', () => {
    moviesQuery.getHasCache.and.returnValue(false);

    const movies$ = service.getAll();
    const expected$ = cold('(x|)', { x: movies });

    expect(movies$).toBeObservable(expected$);
    expect(moviesQuery.getAll).not.toHaveBeenCalled();
    expect(moviesStore.set).toHaveBeenCalledWith(movies);
  });

  it('should not set store and should return all movies when call getAll and have cache', () => {
    moviesQuery.getHasCache.and.returnValue(true);
    moviesQuery.getAll.and.returnValue(movies);

    const movies$ = service.getAll();
    const expected$ = cold('(x|)', { x: movies });

    expect(movies$).toBeObservable(expected$);
    expect(moviesQuery.getAll).toHaveBeenCalled();
    expect(moviesStore.set).not.toHaveBeenCalled();
  });

  it('should return movie and update store when call getByKey', () => {
    moviesQuery.getHasCache.and.returnValue(false);

    const movie$ = service.getByKey('gridiron-gang');
    const expected$ = cold('(x|)', { x: movie4 });

    expect(movie$).toBeObservable(expected$);
    expect(moviesQuery.getEntity).not.toHaveBeenCalled();
    expect(moviesStore.upsert).toHaveBeenCalledWith('gridiron-gang', movie4);
  });

  it('should return movie and not update store when call getByKey and have cache', () => {
    moviesQuery.getHasCache.and.returnValue(true);
    moviesQuery.getEntity.and.returnValue(movie4);

    const movie$ = service.getByKey('gridiron-gang');
    const expected$ = cold('(x|)', { x: movie4 });

    expect(movie$).toBeObservable(expected$);
    expect(moviesQuery.getEntity).toHaveBeenCalledWith('gridiron-gang');
    expect(moviesStore.upsert).not.toHaveBeenCalled();
  });

  it('should throw a error when call getByKey if there is no movie with the key', () => {
    const movie$ = service.getByKey('fake');
    const expected$ = cold(
      '#',
      null,
      new Error(`Movie with key "fake" was not found`)
    );

    expect(movie$).toBeObservable(expected$);
    expect(moviesStore.upsert).not.toHaveBeenCalled();
    expect(moviesQuery.getHasCache).not.toHaveBeenCalled();
    expect(moviesQuery.getEntity).not.toHaveBeenCalled();
  });
});
