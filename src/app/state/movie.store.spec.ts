import { TestBed } from '@angular/core/testing';
import { GenreType } from '../genres/genres.model';
import { MovieStore } from './movie.store';

describe('MovieStore', () => {
  let store: MovieStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieStore]
    });

    store = TestBed.get(MovieStore) as MovieStore;
  });

  it('should create an instance', () => {
    expect(store).toBeTruthy();
  });

  it('should update filter', () => {
    const updateSpy = spyOn(store.ui, 'update');
    store.setFilter('filter');
    store.setFilter('new value');
    store.setFilter('');

    expect(updateSpy.calls.allArgs()).toEqual([
      [{ filter: 'filter' }],
      [{ filter: 'new value' }],
      [{ filter: '' }]
    ]);
  });

  it('should update selected genres', () => {
    const updateSpy = spyOn(store.ui, 'update');
    store.setGenres([GenreType.Action]);
    store.setGenres([GenreType.Action, GenreType.Crime]);
    store.setGenres([]);

    expect(updateSpy.calls.allArgs()).toEqual([
      [{ selectedGenres: ['action'] }],
      [{ selectedGenres: ['action', 'crime'] }],
      [{ selectedGenres: [] }]
    ]);
  });
});
