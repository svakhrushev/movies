import { Injectable } from '@angular/core';
import { GenreType } from '../genres/genres.model';
import { MovieStore } from './movie.store';

@Injectable()
export class MovieUIService {
  constructor(private readonly moviesStore: MovieStore) {}

  setFilter(value: string = ''): void {
    this.moviesStore.setFilter(value);
  }

  setSelectedGenres(value: GenreType[]): void {
    this.moviesStore.setGenres(value);
  }
}
