// tslint:disable: no-magic-numbers no-unsafe-any
import { TestBed } from '@angular/core/testing';
import { RouterQuery } from '@datorama/akita-ng-router-store';
import { cold, getTestScheduler } from 'jasmine-marbles';
import { RouterQueryStub } from '../../testing/router-query.stub';
import { GenreType } from '../genres/genres.model';
import { movie1, movie2, movie3, movie4, movie5, movies } from './movie.mock';
import { Movie } from './movie.model';
import { MovieQuery } from './movie.query';
import { MovieStore } from './movie.store';

describe('MovieQuery', () => {
  let query: MovieQuery;
  let store: MovieStore;
  let routerQuery: RouterQueryStub;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MovieStore,
        { provide: RouterQuery, useClass: RouterQueryStub },
        MovieQuery
      ]
    });

    query = TestBed.get(MovieQuery) as MovieQuery;
    store = TestBed.get(MovieStore) as MovieStore;
    routerQuery = TestBed.get(RouterQuery) as RouterQueryStub;
  });

  it('should create an instance', () => {
    expect(query).toBeTruthy();
  });

  it('should return correct seleted movie', () => {
    store.set(JSON.parse(JSON.stringify(movies)));

    const keys: Record<string, string> = {
      a: 'deadpool',
      b: 'straight-outta-compton',
      c: 'american-gangster',
      d: 'fake'
    };
    const values: Record<string, Movie | undefined> = {
      a: movie1,
      b: movie3,
      c: movie5,
      d: undefined
    };
    /* prettier-ignore */
    const events =   cold('a-b-cd-a|', keys);
    const expected = cold('a-b-cd-a', values);
    const movie$ = query.selectedMovie$;

    events.subscribe((x: string) => {
      routerQuery.setParams(x);
    });

    expect(movie$).toBeObservable(expected);
  });

  it('should return cerrect filter value', () => {
    const filters: Record<string, string> = {
      a: 'filter',
      b: 'new value',
      c: 'test',
      x: ''
    };
    /* prettier-ignore */
    const events =   cold('--a-b-bx-c-a|', filters);
    const expected = cold('x-a-b--x-c-a', filters);
    const filter$ = query.selectFilter();

    events.subscribe((x: string) => {
      store.ui.update({ filter: x });
    });

    expect(filter$).toBeObservable(expected);
  });

  it('should return cerrect selected genres list', () => {
    const genres: Record<string, GenreType[]> = {
      a: [GenreType.Action],
      b: [GenreType.Action, GenreType.Comedy],
      c: [GenreType.Adventure, GenreType.Comedy, GenreType.Mystery],
      x: []
    };
    /* prettier-ignore */
    const events =   cold('--a-b-bx-c-a|', genres);
    const expected = cold('x-a-b--x-c-a', genres);
    const genres$ = query.selectGenres();

    events.subscribe((x: GenreType[]) => {
      store.ui.update({ selectedGenres: x });
    });

    expect(genres$).toBeObservable(expected);
  });

  it('should return correct filtered list of movies', () => {
    getTestScheduler().run(() => {
      store.set(movies);

      const moviesList = {
        a: [movie1, movie2],
        b: [movie1],
        c: [movie2],
        d: [movie4, movie5],
        x: movies,
        z: []
      };
      const filters: Record<string, string> = {
        a: 'dea',
        b: 'deadpool',
        c: 'd',
        d: 'det',
        x: ''
      };
      const genres: Record<string, GenreType[]> = {
        a: [GenreType.Crime],
        b: [GenreType.Biography],
        x: []
      };
      // prettier-ignore
      const f = '-   1s a 1s - 1s - 1s b 1s a 20ms b 1s x 20ms a 1s - 1s - 20ms - 1s - 1s c 1s d'
      // prettier-ignore
      const g = '-   1s - 1s a 1s x 1s - 1s - 20ms - 1s - 20ms - 1s b 1s x 20ms b 1s x 1s - 1s -'
      // prettier-ignore
      const e = 'x 1.2s a 1s c 1s a 1s b 1s - 20ms - 1s - 20ms a 1s z 1s - 20ms - 1s a 1s x 1s d'

      const filter$ = cold(f, filters);
      const ganres$ = cold(g, genres);
      const expected$ = cold(e, moviesList);
      const movies$ = query.selectFilteredMovie();

      filter$.subscribe((x: string) => {
        store.ui.update({ filter: x });
      });
      ganres$.subscribe((x: GenreType[]) => {
        store.ui.update({ selectedGenres: x });
      });

      expect(movies$).toBeObservable(expected$);
    });
  });
  it('should return correct count of movies', () => {
    getTestScheduler().run(() => {
      store.set(movies);

      const count = {
        a: 2,
        b: 1,
        c: 1,
        d: 2,
        x: 5,
        z: 0
      };
      const filters: Record<string, string> = {
        a: 'dea',
        b: 'deadpool',
        c: 'd',
        d: 'det',
        x: ''
      };
      const genres: Record<string, GenreType[]> = {
        a: [GenreType.Crime],
        b: [GenreType.Biography],
        x: []
      };
      // prettier-ignore
      const f = '-   1s a 1s - 1s - 1s b 1s a 20ms b 1s x 20ms a 1s - 1s - 20ms - 1s - 1s c 1s d'
      // prettier-ignore
      const g = '-   1s - 1s a 1s x 1s - 1s - 20ms - 1s - 20ms - 1s b 1s x 20ms b 1s x 1s - 1s -'
      // prettier-ignore
      const c = 'x 1.2s a 1s c 1s a 1s b 1s - 20ms - 1s - 20ms a 1s z 1s - 20ms - 1s a 1s x 1s d'

      const filter$ = cold(f, filters);
      const ganres$ = cold(g, genres);
      const expectedCount$ = cold(c, count);
      const count$ = query.selectFilteredMovieCount();

      filter$.subscribe((x: string) => {
        store.ui.update({ filter: x });
      });
      ganres$.subscribe((x: GenreType[]) => {
        store.ui.update({ selectedGenres: x });
      });

      expect(count$).toBeObservable(expectedCount$);
    });
  });
});
