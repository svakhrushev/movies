// tslint:disable: object-literal-sort-keys
import { GenreType } from '../genres/genres.model';
import { Movie } from './movie.model';

export const movie1: Movie = {
  id: 1,
  key: 'deadpool',
  name: 'Deadpool',
  description:
    'A former Special Forces operative turned mercenary is subjected to a rogue experiment that leaves him with accelrated healing powers, adopting the alter ego Deadpool.',
  genres: [GenreType.Action, GenreType.Adventure, GenreType.Comedy],
  rate: '8.6',
  length: '1hr 48mins',
  img: 'deadpool.jpg'
};

export const movie2: Movie = {
  id: 2,
  key: 'we-are-the-millers',
  name: "We're the Millers",
  description:
    'A veteran pot dealer creates a fake family as part of his plan to move a huge shipment of weed into the U.S. from Mexico.',
  genres: [GenreType.Adventure, GenreType.Comedy, GenreType.Crime],
  rate: '7.0',
  length: '1hr 50mins',
  img: 'we-are-the-millers.jpg'
};

export const movie3: Movie = {
  id: 3,
  key: 'straight-outta-compton',
  name: 'Straight Outta Compton',
  description:
    'The group NWA emerges from the mean streets of Compton in Los Angeles, California, in the mid-1980s and revolutionizes Hip Hop culture with their music and tales about life in the hood.',
  genres: [GenreType.Biography, GenreType.Drama, GenreType.History],
  rate: '8.0',
  length: '2hr 27mins',
  img: 'straight-outta-compton.jpg'
};

export const movie4: Movie = {
  id: 4,
  key: 'gridiron-gang',
  name: 'Gridiron Gang',
  description:
    'Teenagers at a juvenile detention center, under the leadership of their counselor, gain self-esteem by playing football together.',
  genres: [GenreType.Crime, GenreType.Drama, GenreType.Sport],
  rate: '6.9',
  length: '2hr 5mins',
  img: 'gridiron-gang.jpg'
};

export const movie5: Movie = {
  id: 5,
  key: 'american-gangster',
  name: 'American Gangster',
  description:
    'In 1970s America, a detective works to bring down the drug empire of Frank Lucas, a heroin kingpin from Manhattan, who is smuggling the drug into the country from the Far East.',
  genres: [GenreType.Biography, GenreType.Crime, GenreType.Drama],
  rate: '7.8',
  length: '2hr 37mins',
  img: 'american-gangster.jpg'
};

export const movies = [movie1, movie2, movie3, movie4, movie5];
