import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MovieQuery } from './movie.query';
import { MovieService } from './movie.service';
import { MovieStore } from './movie.store';
import { MovieUIService } from './movie.ui.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [MovieQuery, MovieService, MovieStore, MovieUIService]
})
export class StateModule {}
