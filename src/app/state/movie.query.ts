import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { RouterQuery } from '@datorama/akita-ng-router-store';
import { combineLatest, Observable } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  share,
  startWith,
  switchMap
} from 'rxjs/operators';
import { GenreType } from '../genres/genres.model';
import { Movie } from './movie.model';
import { MovieState, MovieStore, MovieUI } from './movie.store';

const SEARCH_DEBOUNCE_TIME = 200;

@Injectable()
export class MovieQuery extends QueryEntity<MovieState> {
  selectedMovie$ = this.routerQuery
    .selectParams('key')
    .pipe(switchMap(id => this.selectEntity(id)));

  constructor(
    protected store: MovieStore,
    private readonly routerQuery: RouterQuery
  ) {
    super(store);
    this.createUIQuery();
  }

  selectFilteredMovie(): Observable<Movie[]> {
    return this.selectFilters().pipe(
      switchMap(([filter, genres]) =>
        this.selectAll({
          filterBy: entity => this.moviesFilter(entity, filter, genres)
        })
      ),
      distinctUntilChanged((a, b) => this.compareMoviesLists(a, b))
    );
  }

  selectFilteredMovieCount(): Observable<number> {
    return this.selectFilters().pipe(
      switchMap(([filter, genres]) =>
        this.selectCount(entity => this.moviesFilter(entity, filter, genres))
      ),
      distinctUntilChanged()
    );
  }

  selectFilter(): Observable<string> {
    return this.ui.select<string>((store: MovieUI) => store.filter);
  }

  selectGenres(): Observable<GenreType[]> {
    return this.ui.select<GenreType[]>(
      (store: MovieUI) => store.selectedGenres
    );
  }

  private compareMoviesLists(list1: Movie[], list2: Movie[]): boolean {
    const key1 = list1.map(movie => movie.key).toString();
    const key2 = list2.map(movie => movie.key).toString();
    return key1 === key2;
  }

  private getFilter(): string {
    return (this.ui.getValue() as MovieUI).filter;
  }

  private getGenres(): GenreType[] {
    return (this.ui.getValue() as MovieUI).selectedGenres;
  }

  private selectFilters(): Observable<[string, GenreType[]]> {
    const search$ = this.selectDebounceFilter();
    const filter$ = this.selectDebounceGenres();
    return combineLatest([search$, filter$]).pipe(share());
  }

  private selectDebounceFilter(): Observable<string> {
    return this.selectFilter().pipe(
      debounceTime(SEARCH_DEBOUNCE_TIME),
      distinctUntilChanged(),
      startWith(this.getFilter()),
      share()
    );
  }

  private selectDebounceGenres(): Observable<GenreType[]> {
    return this.selectGenres().pipe(
      debounceTime(SEARCH_DEBOUNCE_TIME),
      distinctUntilChanged((a, b) => a.toString() === b.toString()),
      startWith(this.getGenres()),
      share()
    );
  }

  private moviesFilter(
    movie: Movie,
    filter: string,
    genres: GenreType[]
  ): boolean {
    const isNameConsist = movie.name
      .toLowerCase()
      .includes(filter.toLowerCase());

    const isDescriptionConsist = movie.description
      .toLowerCase()
      .includes(filter.toLowerCase());

    const hasGenre =
      genres.length === 0 ||
      movie.genres.filter(genre => genres.includes(genre)).length > 0;

    return (isNameConsist || isDescriptionConsist) && hasGenre;
  }
}
