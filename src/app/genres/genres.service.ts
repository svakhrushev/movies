import { Injectable } from '@angular/core';
import { GenreType } from './genres.model';

type Genres = keyof typeof GenreType;
type GenreEntry = { key: Genres; value: GenreType };
type GenresMap = Record<GenreType, Genres>;

@Injectable({
  providedIn: 'root'
})
export class GenresService {
  readonly genresEntries = this.getGenresEntries();
  readonly genresMap = this.getGenresMap();

  getGenre(genre: GenreType): Genres {
    return this.genresMap[genre];
  }

  private getGenresEntries(): GenreEntry[] {
    return Object.entries(GenreType).map(entry => ({
      key: entry[0],
      value: entry[1]
    })) as GenreEntry[];
  }

  private getGenresMap(): GenresMap {
    // tslint:disable-next-line:no-object-literal-type-assertion
    const result = {} as GenresMap;
    this.genresEntries.forEach(type => {
      const key = type.value;
      result[key] = type.key;
    });

    return result;
  }
}
