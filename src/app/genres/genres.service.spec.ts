import { TestBed } from '@angular/core/testing';
import { GenreType } from './genres.model';
import { GenresService } from './genres.service';

describe('GenresService', () => {
  let service: GenresService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(GenresService) as GenresService;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return correct genre', () => {
    expect(service.getGenre(GenreType.Action)).toBe('Action');
    expect(service.getGenre(GenreType.Drama)).toBe('Drama');
  });
});
