import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterOutlet } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [RouterTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  }));

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should return animation data', () => {
    const data = { animation: 'MoviesPage' };
    // tslint:disable-next-line:no-object-literal-type-assertion
    const outlet = { activatedRouteData: data } as Partial<RouterOutlet>;

    expect(app.prepareRoute(outlet as RouterOutlet)).toBe('MoviesPage');
  });
});
