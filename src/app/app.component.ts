import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from './app.component.animation';

@Component({
  animations: [slideInAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-root',
  styleUrls: ['./app.component.sass'],
  templateUrl: './app.component.html'
})
export class AppComponent {
  // tslint:disable-next-line:no-any
  prepareRoute(outlet?: RouterOutlet): any {
    return (
      outlet &&
      outlet.activatedRouteData &&
      // tslint:disable-next-line: no-string-literal
      outlet.activatedRouteData['animation']
    );
  }
}
