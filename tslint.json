{
  "defaultSeverity": "error",
  "jsRules": {},
  "rules": {
    "component-class-suffix": true,
    "contextual-lifecycle": true,
    "directive-class-suffix": true,
    "directive-selector": [
      true,
      "attribute",
      "app",
      "camelCase"
    ],
    "component-selector": [
      true,
      "element",
      "app",
      "kebab-case"
    ],
    /* ------- TypeScript-specific ------- */

    "adjacent-overload-signatures": true,
    // "ban-ts-ignore": true,
    // "ban-types": true,
    "member-access": [true, "no-public"],
    "member-ordering": [
      true,
      {
        "order": [
          "public-static-field",
          "protected-static-field",
          "protected-static-method",
          "public-static-method",
          "private-static-field",
          "private-static-method",
          "public-instance-field",
          "protected-instance-field",
          "private-instance-field",
          "public-constructor",
          "protected-constructor",
          "private-constructor",
          "public-instance-method",
          "protected-instance-method",
          "private-instance-method"
        ]
      }
    ],
    "no-any": true,
    "no-empty-interface": true,
    "no-for-in": true,
    // "no-import-side-effect": true,
    "no-inferrable-types": [true, "ignore-params", "ignore-properties"],
    "no-internal-module": true,
    "no-magic-numbers": true,
    "no-namespace": [true, "allow-declarations"],
    "no-non-null-assertion": true,
    "no-parameter-reassignment": true,
    "no-reference": true,
    "no-unnecessary-type-assertion": true,
    "no-var-requires": true,
    "only-arrow-functions": [
      true,
      "allow-declarations",
      "allow-named-functions"
    ],
    "prefer-for-of": true,
    // "promise-function-async": true,
    "typedef": [true, "call-signature", "parameter", "property-declaration"],
    "unified-signatures": true,

    /* ------- Functionality ------- */

    // "await-promise": true,
    "ban-comma-operator": true,
    // "ban": true,
    "curly": true,
    "forin": true,
    "function-constructor": true,
    // "import-blacklist": [true, "rxjs", "lodash"],
    "label-position": true,
    "no-arg": true,
    "no-async-without-await": true,
    "no-bitwise": true,
    "no-conditional-assignment": true,
    "no-console": [true, "log"],
    "no-construct": true,
    "no-debugger": true,
    "no-duplicate-super": true,
    "no-duplicate-switch-case": true,
    "no-duplicate-variable": [true, "check-parameters"],
    "no-dynamic-delete": true,
    "no-empty": true,
    "no-eval": true,
    // "no-floating-promises": true,
    "no-for-in-array": true,
    "no-implicit-dependencies": [true, "dev"],
    "no-inferred-empty-object-type": true,
    "no-invalid-template-strings": true,
    "no-invalid-this": true,
    "no-misused-new": true,
    // "no-null-keyword": true,
    "no-null-undefined-union": true,
    "no-object-literal-type-assertion": true,
    "no-promise-as-boolean": true,
    "no-return-await": true,
    "no-shadowed-variable": true,
    "no-sparse-arrays": true,
    "no-string-literal": true,
    "no-string-throw": true,
    // "no-submodule-imports": true,
    "no-switch-case-fall-through": true,
    "no-tautology-expression": true,
    "no-this-assignment": [true, { "allow-destructuring": true }],
    "no-unbound-method": [true, "ignore-static"],
    "no-unnecessary-class": [
      true,
      "allow-empty-class",
      "allow-constructor-only",
      "allow-static-only"
    ],
    "no-unsafe-any": true,
    "no-unsafe-finally": true,
    "no-unused-expression": [true, "allow-fast-null-checks"],
    // "no-unused-variable": true,
    // "no-use-before-declare": true,
    "no-var-keyword": true,
    "no-void-expression": true,
    "prefer-conditional-expression": [true, "check-else-if"],
    "prefer-object-spread": true,
    "radix": true,
    "restrict-plus-operands": true,
    "static-this": true,
    "strict-boolean-expressions": [
      true,
      "allow-null-union",
      "allow-undefined-union",
      "ignore-rhs"
    ],
    "strict-comparisons": true,
    "strict-string-expressions": [true, { "allow-empty-types": true }],
    "strict-type-predicates": true,
    "switch-default": true,
    "triple-equals": true,
    // "typeof-compare": true,
    "unnecessary-constructor": [true, { "check-super-calls": true }],
    "use-default-type-parameter": true,
    "use-isnan": true,

    /* ------- Maintainability ------- */

    "cyclomatic-complexity": true,
    "deprecation": true,
    "invalid-void": [true, { "allow-generics": false }],
    "max-classes-per-file": [true, 1],
    "max-file-line-count": [true, 300],
    // "no-default-export": true,
    // "no-default-import": true,
    "no-duplicate-imports": true,
    // "no-mergeable-namespace": true,
    // "no-require-imports": true,
    "object-literal-sort-keys": [
      true,
      "ignore-blank-lines",
      "ignore-case",
      "locale-compare",
      "shorthand-first"
    ],
    "prefer-const": [true, { "destructuring": "all" }],
    "prefer-readonly": true,

    /* Style */
    "array-type": [true, "array-simple"],
    "interface-name": [true, "never-prefix"],
    "ordered-imports": [
      true,
      {
        "import-sources-order": "lowercase-last",
        "named-imports-order": "case-insensitive"
      }
    ],
    
    /* Format */
    "arrow-parens": [true, "ban-single-arg-parens"]

    // style's and format's rules are implemented by Prettier
  },
  "rulesDirectory": []
}
