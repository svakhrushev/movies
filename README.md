# Movies
**Version: 1.5**

## Description
"Movies" is a new amazing application that helps you to find that you need.

**It has the next incredible features:**

- Simple desing

- Reactive search

- Smart filtering

## Installation

1. **NodeJS instalation**

	If there is no instaled NodeJS on your computer just [donwnload](https://nodejs.org/) the LTS version and install it.

2. **Download the project**

	Choose a folder on your computer and download or clone the project using Git from [Movie repo](https://bitbucket.org/svakhrushev/movies/)

3. **Download the dependencies**

	Open the root folder of the project in cli and run `npm install`

4. **Launch the application**

	Just run `npm start` from root folder of the project

5. **Build the project**

	Run `npm run build` for building the project
